import { Order } from "./order";

export class PointOfDelivery {

  constructor(public id: number, public address: string,
    public orders: Order[]) {}

}
