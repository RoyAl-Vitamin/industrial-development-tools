import { Order } from "./order";
import { Product } from "./product";

export class OrderPos {

  constructor(public id: number, public price: number, public discount: number,
    public quantity: number, public description: string, public order: Order,
    public products: Product) {}

}
