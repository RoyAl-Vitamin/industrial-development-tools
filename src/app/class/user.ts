import { Order } from "./order";

export class User {

  constructor(public id: number, public fullname: string, public order: Order[]) { }

}
