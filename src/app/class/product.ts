import { GroupProduct } from "./group-product";
import { OrderPos } from "./order-pos";

export class Product {

  constructor(public id: number, public name: string,
    public group: GroupProduct, public position: OrderPos) {}

}
