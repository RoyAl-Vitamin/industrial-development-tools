import { User } from "./user";
import { OrderPos } from "./order-pos";

export class Order {

  constructor(public id: number, public orderPoses: OrderPos, public user: User,
    public point: any) {}

}
