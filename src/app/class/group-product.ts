import { Product } from "./product";

export class GroupProduct {

  constructor(public id: number, public name: string,
    public products: Product[]) {}

}
