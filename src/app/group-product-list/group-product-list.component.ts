import { Component, OnInit } from '@angular/core';
import { GroupProductEndpointService } from '../service/group-product-endpoint.service';
import { GroupProduct } from '../class/group-product';

@Component({
  selector: 'app-group-product-list',
  templateUrl: './group-product-list.component.html',
  styleUrls: ['./group-product-list.component.css'],
  providers: [GroupProductEndpointService]
})
export class GroupProductListComponent implements OnInit {

    groupProducts: GroupProduct[] = [];

    constructor(private groupProductService: GroupProductEndpointService) { }

    ngOnInit() {
      this.groupProductService.getData().subscribe((data: GroupProduct[]) => this.groupProducts = data);
    }
}
