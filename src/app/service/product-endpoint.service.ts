import { Injectable } from '@angular/core';
import { UrlEndpointService } from './url-endpoint.service';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';

import { Product } from '../class/product';

@Injectable({
  providedIn: 'root'
})
export class ProductEndpointService {

  private path: string = "/product/";

  constructor(private http: HttpClient, private urlService: UrlEndpointService) { }

  getData() {
      return this.http.get<Product[]>(this.urlService.getData() + this.path);
  }

  getProduct(id: number): Observable<HttpResponse<Product>> {
      return this.http.get<Product>(this.urlService.getData() + this.path + id, { observe: 'response' });
  }

  /*
   * Возвращаем полный ответ от сервера, для дальнейшего чтения заголовков
   */
  getProducts(): Observable<HttpResponse<Product[]>> {
      return this.http.get<Product[]>(this.urlService.getData() + this.path, { observe: 'response' });
  }

  /**
   * Удаляет пользователя
   */
  deleteProduct(id: number): Observable<{}> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    const url = this.urlService.getData() + this.path + `${id}`;
    return this.http.delete(url, httpOptions);
  }

  /**
   * Сбрасывает изменения в пользователе
   */
  saveProduct(id: number, product: Product): Observable<{}> {
    const url = this.urlService.getData() + this.path + `${id}`;
    return this.http.put(url, product);
  }

  /**
   * Вставляет в БД нового пользователя, его ID не важен
   * в хеере возвращается относительная ссылка на добавленного пользователя
   */
  addProduct(product: Product): Observable<HttpResponse<{}>> {
    const url = this.urlService.getData() + this.path;
    return this.http.post(url, product, { observe: 'response' });
  }
}
