import { TestBed } from '@angular/core/testing';

import { ProductEndpointService } from './product-endpoint.service';

describe('ProductEndpointService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductEndpointService = TestBed.get(ProductEndpointService);
    expect(service).toBeTruthy();
  });
});
