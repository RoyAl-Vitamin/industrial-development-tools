import { TestBed } from '@angular/core/testing';

import { GroupProductEndpointService } from './group-product-endpoint.service';

describe('GroupProductEndpointService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GroupProductEndpointService = TestBed.get(GroupProductEndpointService);
    expect(service).toBeTruthy();
  });
});
