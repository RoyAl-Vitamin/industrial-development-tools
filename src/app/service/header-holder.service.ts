import { Injectable } from '@angular/core';
import { HeaderName } from '../class/header-name';

@Injectable({
  providedIn: 'root'
})
export class HeaderHolderService {

  constructor() { }

  public readonly projectName: string = "industrial-development-tools";
  //"Welcome to <a href=\"https://github.com/RoyAl-Vitamin/industrial-development-tools\">industrial-development-tools</a>!";

  private data: HeaderName = { header: this.projectName };

  getData(): string {
      return this.data.header;
  }

  setData(header: string) {
      this.data = new HeaderName(header);
  }

}
