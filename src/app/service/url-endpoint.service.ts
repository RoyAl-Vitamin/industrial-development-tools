import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UrlEndpointService {

  constructor() {
    console.log("URL == " + this.url);
    if (this.url === undefined) {
      this.url = localStorage.getItem("url");
      console.log("URL == " + this.url);
      if (this.url === undefined || this.url === null) {
        this.url = "http://localhost:8080";
        console.log("URL == " + this.url);
      }
    }
  }

  private url: string;

  getData(): string {
    return this.url;
  }

  setData(new_url: string): void {
    this.url = new_url;
    localStorage.setItem("url", this.url);
  }
}
