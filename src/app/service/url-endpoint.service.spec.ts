import { TestBed } from '@angular/core/testing';

import { UrlEndpointService } from './url-endpoint.service';

describe('UrlEndpointService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UrlEndpointService = TestBed.get(UrlEndpointService);
    expect(service).toBeTruthy();
  });
});
