import { TestBed } from '@angular/core/testing';

import { HeaderHolderService } from './header-holder.service';

describe('HeaderHolderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HeaderHolderService = TestBed.get(HeaderHolderService);
    expect(service).toBeTruthy();
  });
});
