import { TestBed } from '@angular/core/testing';

import { PointOfDeliveryEndpointService } from './point-of-delivery-endpoint.service';

describe('PointOfDeliveryEndpointService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PointOfDeliveryEndpointService = TestBed.get(PointOfDeliveryEndpointService);
    expect(service).toBeTruthy();
  });
});
