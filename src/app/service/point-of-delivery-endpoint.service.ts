import { Injectable } from '@angular/core';
import { UrlEndpointService } from './url-endpoint.service';
import { HttpClient } from '@angular/common/http';

import { PointOfDelivery } from '../class/point-of-delivery';

@Injectable({
  providedIn: 'root'
})
export class PointOfDeliveryEndpointService {

  constructor(private http: HttpClient, private urlService: UrlEndpointService) { }

  getData() {
      return this.http.get<PointOfDelivery[]>(this.urlService.getData() + "/pointOfDelivery/");
  }

}
