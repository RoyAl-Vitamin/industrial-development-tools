import { Injectable } from '@angular/core';
import { UrlEndpointService } from './url-endpoint.service';
import { HttpClient } from '@angular/common/http';

import { GroupProduct } from '../class/group-product';

@Injectable({
  providedIn: 'root'
})
export class GroupProductEndpointService {

  constructor(private http: HttpClient, private urlService: UrlEndpointService) { }

  getData() {
      return this.http.get<GroupProduct[]>(this.urlService.getData() + "/group/");
  }
}
