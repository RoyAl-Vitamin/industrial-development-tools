import { Injectable } from '@angular/core';
import { UrlEndpointService } from './url-endpoint.service';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';

import { User } from '../class/user';

@Injectable({
  providedIn: 'root'
})
export class UserEndpointService {

  private path: string = "/user/";

  constructor(private http: HttpClient, private urlService: UrlEndpointService) { }

  getUser(id: number): Observable<HttpResponse<User>> {
      return this.http.get<User>(this.urlService.getData() + this.path + id, { observe: 'response' });
  }

  /*
   * Возвращаем полный ответ от сервера, для дальнейшего чтения заголовков
   */
  getUsers(): Observable<HttpResponse<User[]>> {
      return this.http.get<User[]>(this.urlService.getData() + this.path, { observe: 'response' });
  }

  /**
   * Удаляет пользователя
   */
  deleteUser(id: number): Observable<{}> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    const url = this.urlService.getData() + this.path + `${id}`;
    return this.http.delete(url, httpOptions);
  }

  /**
   * Сбрасывает изменения в пользователе
   */
  saveUser(id: number, user: User): Observable<{}> {
    const url = this.urlService.getData() + this.path + `${id}`;
    return this.http.put(url, user);
  }

  /**
   * Вставляет в БД нового пользователя, его ID не важен
   * в хеере возвращается относительная ссылка на добавленного пользователя
   */
  addUser(user: User): Observable<HttpResponse<{}>> {
    const url = this.urlService.getData() + this.path;
    return this.http.post(url, user, { observe: 'response' });
  }
}
