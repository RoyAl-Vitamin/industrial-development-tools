import { Component, OnInit } from '@angular/core';

import { PointOfDelivery } from '../class/point-of-delivery';
import { PointOfDeliveryEndpointService } from '../service/point-of-delivery-endpoint.service';

@Component({
  selector: 'app-point-of-delivery-list',
  templateUrl: './point-of-delivery-list.component.html',
  styleUrls: ['./point-of-delivery-list.component.css']
})
export class PointOfDeliveryListComponent implements OnInit {

  points: PointOfDelivery[] = [];

  constructor(private pointService: PointOfDeliveryEndpointService) { }

  ngOnInit() {
    this.pointService.getData().subscribe((data: PointOfDelivery[]) => this.points = data);
  }

}
