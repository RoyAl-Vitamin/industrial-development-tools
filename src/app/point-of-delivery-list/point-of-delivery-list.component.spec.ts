import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PointOfDeliveryListComponent } from './point-of-delivery-list.component';

describe('PointOfDeliveryListComponent', () => {
  let component: PointOfDeliveryListComponent;
  let fixture: ComponentFixture<PointOfDeliveryListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PointOfDeliveryListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PointOfDeliveryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
