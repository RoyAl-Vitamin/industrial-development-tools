import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ProductEndpointService } from '../service/product-endpoint.service';
import { HeaderHolderService } from '../service/header-holder.service';

import { Product } from '../class/product';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
  providers: [ProductEndpointService]
})
export class ProductListComponent implements OnInit {

  products: Product[] = [];

  constructor(private productService: ProductEndpointService,
    private headerService: HeaderHolderService,
    private router: Router) { }

  ngOnInit() {
    this.productService.getData().subscribe((data: Product[]) => this.products = data);
  }

  refresh() {
    this.productService.getProducts().subscribe(resp => {
      this.products = resp.body;
    });
  }

  delete(id: number) {
    console.log("delete product with id == " + id);
    this.productService.deleteProduct(id).subscribe(() => {this.refresh();});
  }

  setting(id: number) {
    console.log("setting user with id == " + id);
    this.router.navigate(['/product/' + id]);
  }
}
