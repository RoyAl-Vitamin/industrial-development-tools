import { Component, OnInit } from '@angular/core';
import { HeaderHolderService } from '../service/header-holder.service';
import { UrlEndpointService } from '../service/url-endpoint.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [HeaderHolderService]
})
export class HeaderComponent implements OnInit {

  newUrl: string;

  title: string;

  constructor(private headerService: HeaderHolderService,
    private urlEndpointService: UrlEndpointService) {
      this.newUrl = this.urlEndpointService.getData();
    }

  ngOnInit() {
    this.title = this.headerService.getData();
  }

  setMainEndpoint() {
    this.urlEndpointService.setData(this.newUrl);
  }

}
