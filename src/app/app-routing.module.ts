import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserAddComponent } from './user-add/user-add.component';
import { ProductListComponent } from './product-list/product-list.component';
import { GroupProductListComponent } from './group-product-list/group-product-list.component';
import { PointOfDeliveryListComponent } from './point-of-delivery-list/point-of-delivery-list.component';

const routes: Routes = [
  { path: 'user', component: UserListComponent },
  { path: 'user/add', component: UserAddComponent },
  { path: 'user/:id', component: UserDetailComponent },
  { path: 'product', component: ProductListComponent },
  { path: 'group', component: GroupProductListComponent },
  { path: 'point', component: PointOfDeliveryListComponent },
  { path: '**', component: PageNotFoundComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
