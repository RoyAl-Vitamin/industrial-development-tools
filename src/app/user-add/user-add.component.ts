import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { UserEndpointService } from '../service/user-endpoint.service';
import { HeaderHolderService } from '../service/header-holder.service';
import { User } from '../class/user';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css']
})
export class UserAddComponent implements OnInit {

  user: User = new User(0, "", undefined);

  constructor(private userService: UserEndpointService,
    private headerService: HeaderHolderService,
    private router: Router) { }

  ngOnInit() {
    console.log("fullname == " + this.user.fullname);
  }

  addUser() {
    this.userService.addUser(this.user).subscribe(response => {
      console.log("Location: " + response.headers.get("Location"));
    });

    // this.router.navigate(['/user']);
  }

  goBack() {
    this.router.navigate(['/user']);
  }

}
