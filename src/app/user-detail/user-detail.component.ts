import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { map, switchMap, filter } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { UserEndpointService } from '../service/user-endpoint.service';
import { User } from '../class/user';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  // private user: Observable<User>;
  private user: User;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserEndpointService
  ) {}

  ngOnInit() {
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        // console.log("id == " + params.get('id'));
        return this.userService.getUser(+params.get('id')).pipe(map((response: HttpResponse<User>) => {
          console.log("id == " + response.body.id);
          console.log("fullname == " + response.body.fullname);
          return response.body;
        }));
      })
    ).subscribe(userResp => {
      this.user = userResp;
    });
  }

  refresh() {
    this.userService.getUser(this.user.id).subscribe(resp => {
      this.user = resp.body;
    });
  }

  save(newFullname: string) {
    console.log(newFullname);
    this.user.fullname = newFullname;
    this.userService.saveUser(this.user.id, this.user);
  }

  goBack() {
    this.router.navigate(['/user']);
  }

}
