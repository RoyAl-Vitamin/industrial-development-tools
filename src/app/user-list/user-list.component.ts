import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserEndpointService } from '../service/user-endpoint.service';
import { HeaderHolderService } from '../service/header-holder.service';
import { User } from '../class/user';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
  providers: [UserEndpointService, HeaderHolderService]
})
export class UserListComponent implements OnInit {

  readonly componentName: string = "Список пользователей";
  users: User[] = [];

  constructor(private userService: UserEndpointService,
    private headerService: HeaderHolderService,
    private router: Router) { }

  ngOnInit() {
    this.refresh();
    this.headerService.setData(this.componentName);
  }

  refresh() {
    this.userService.getUsers().subscribe(resp => {
      this.users = resp.body;
    });
  }

  delete(id: number) {
    console.log("delete user with id == " + id);
    this.userService.deleteUser(id).subscribe(() => {this.refresh();});
  }

  setting(id: number) {
    console.log("setting user with id == " + id);
    this.router.navigate(['/user/' + id]);
  }

  addUser() {
    this.router.navigate(['/user/add']);
  }

}
